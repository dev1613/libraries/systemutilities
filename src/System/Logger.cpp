/**
 * @file Logger.cpp
 *
 * @brief Classes to represent debug logger
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <cstdarg>
#include <memory>
#include <sstream>

#include <System/Logger.h>

// Constants
static const std::string STR_DEBUG("DEBUG");
static const std::string STR_INFO("INFO");
static const std::string STR_WARN("WARN");
static const std::string STR_ERROR("ERROR");
static const std::string STR_FATAL("FATAL");

namespace System
{
  /////////////////////////////////////////////////////////////////////////////
  // GetLogLevel
  /////////////////////////////////////////////////////////////////////////////
  bool GetLogLevel(LogLevel& level, const std::string& levelStr)
  {
    bool valid = true;
    if(levelStr == STR_DEBUG)
    {
      level = LOG_DEBUG;
    }
    else if(levelStr == STR_INFO)
    {
      level = LOG_INFO;
    }
    else if(levelStr == STR_WARN)
    {
      level = LOG_WARN;
    }
    else if(levelStr == STR_ERROR)
    {
      level = LOG_ERROR;
    }
    else if(levelStr == STR_FATAL)
    {
      level = LOG_FATAL;
    }
    else
    {
      valid = false;
    }

    return valid;
  }

  /////////////////////////////////////////////////////////////////////////////
  // operator<<
  /////////////////////////////////////////////////////////////////////////////
  std::ostream& operator<<(std::ostream& stream, LogLevel level)
  {
    switch(level)
    {
      case LOG_DEBUG:
        stream << STR_DEBUG;
        break;
      case LOG_INFO:
        stream << STR_INFO;
        break;
      case LOG_WARN:
        stream << STR_WARN;
        break;
      case LOG_ERROR:
        stream << STR_ERROR;
        break;
      case LOG_FATAL:
        stream << STR_FATAL;
        break;
      default:
        stream << "UNKNOWN LEVEL";
        break;
    }

    return stream;
  }

  /////////////////////////////////////////////////////////////////////////////
  // Constructor
  /////////////////////////////////////////////////////////////////////////////
  Logger::Logger(std::ostream& out, LogLevel level) : mOut(out), mLevel(level)
  {
  }

  /////////////////////////////////////////////////////////////////////////////
  // log
  /////////////////////////////////////////////////////////////////////////////
  void Logger::log(LogLevel level, const char* format, ...)
  {
    if(level >= mLevel)
    {
      va_list args;
      va_start(args, format);
      size_t len = std::vsnprintf(nullptr, 0, format, args) + 1;
      va_end(args);

      std::unique_ptr<char> buffer(new char[len]);
      va_start(args, format);
      std::vsnprintf(buffer.get(), len, format, args);
      va_end(args);

      std::stringstream stream;
      stream << level << ": " << buffer.get() << std::endl;
      mOut << stream.str();
    }
  }

} // namespace System
