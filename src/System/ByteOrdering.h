/**
 * @file ByteOrdering.h
 *
 * @brief Helper functions for byte ordering
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <cstdint>

namespace ByteOrdering
{
  /////////////////////////////////////////////////////////////////////////////
  /// @brief Converts the big endian value to host
  /// @param value - big endian value to convert
  /// @return - value in host endian-ness
  /////////////////////////////////////////////////////////////////////////////
  bool FromBigEndian(bool value);
  uint8_t FromBigEndian(uint8_t value);
  int8_t FromBigEndian(int8_t value);
  uint16_t FromBigEndian(uint16_t value);
  int16_t FromBigEndian(int16_t value);
  uint32_t FromBigEndian(uint32_t value);
  int32_t FromBigEndian(int32_t value);
  uint64_t FromBigEndian(uint64_t value);
  int64_t FromBigEndian(int64_t value);
  float FromBigEndian(float value);
  double FromBigEndian(double value);

  /////////////////////////////////////////////////////////////////////////////
  /// @brief Converts the little endian value to host
  /// @param value - little endian value to convert
  /// @return - value in host endian-ness
  /////////////////////////////////////////////////////////////////////////////
  bool FromLittleEndian(bool value);
  uint8_t FromLittleEndian(uint8_t value);
  int8_t FromLittleEndian(int8_t value);
  uint16_t FromLittleEndian(uint16_t value);
  int16_t FromLittleEndian(int16_t value);
  uint32_t FromLittleEndian(uint32_t value);
  int32_t FromLittleEndian(int32_t value);
  uint64_t FromLittleEndian(uint64_t value);
  int64_t FromLittleEndian(int64_t value);
  float FromLittleEndian(float value);
  double FromLittleEndian(double value);

  /////////////////////////////////////////////////////////////////////////////
  /// @brief Converts the host value to big endian
  /// @param value - value to convert
  /// @return - value in big endian
  /////////////////////////////////////////////////////////////////////////////
  bool ToBigEndian(bool value);
  uint8_t ToBigEndian(uint8_t value);
  int8_t ToBigEndian(int8_t value);
  uint16_t ToBigEndian(uint16_t value);
  int16_t ToBigEndian(int16_t value);
  uint32_t ToBigEndian(uint32_t value);
  int32_t ToBigEndian(int32_t value);
  uint64_t ToBigEndian(uint64_t value);
  int64_t ToBigEndian(int64_t value);
  float ToBigEndian(float value);
  double ToBigEndian(double value);

  /////////////////////////////////////////////////////////////////////////////
  /// @brief Converts the host value to little endian
  /// @param value - value to convert
  /// @return - value in little endian
  /////////////////////////////////////////////////////////////////////////////
  bool ToLittleEndian(bool value);
  uint8_t ToLittleEndian(uint8_t value);
  int8_t ToLittleEndian(int8_t value);
  uint16_t ToLittleEndian(uint16_t value);
  int16_t ToLittleEndian(int16_t value);
  uint32_t ToLittleEndian(uint32_t value);
  int32_t ToLittleEndian(int32_t value);
  uint64_t ToLittleEndian(uint64_t value);
  int64_t ToLittleEndian(int64_t value);
  float ToLittleEndian(float value);
  double ToLittleEndian(double value);

} // namespace ByteOrdering
