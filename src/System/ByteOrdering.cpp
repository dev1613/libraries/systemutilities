/**
 * @file ByteOrdering.cpp
 *
 * @brief Helper functions for byte ordering
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

// Includes
#include <System/ByteOrdering.h>

namespace ByteOrdering
{
  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (bool)
  /////////////////////////////////////////////////////////////////////////////
  bool FromBigEndian(bool value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (uint8_t)
  /////////////////////////////////////////////////////////////////////////////
  uint8_t FromBigEndian(uint8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (int8_t)
  /////////////////////////////////////////////////////////////////////////////
  int8_t FromBigEndian(int8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (uint16_t)
  /////////////////////////////////////////////////////////////////////////////
  uint16_t FromBigEndian(uint16_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint16_t>(bytes[0]) << 8) |
      (static_cast<uint16_t>(bytes[1]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (int16_t)
  /////////////////////////////////////////////////////////////////////////////
  int16_t FromBigEndian(int16_t value)
  {
    uint16_t* uvalue = reinterpret_cast<uint16_t*>(&value);
    *uvalue = FromBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (uint32_t)
  /////////////////////////////////////////////////////////////////////////////
  uint32_t FromBigEndian(uint32_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint32_t>(bytes[0]) << 24) |
      (static_cast<uint32_t>(bytes[1]) << 16) |
      (static_cast<uint32_t>(bytes[2]) << 8) |
      (static_cast<uint32_t>(bytes[3]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (int32_t)
  /////////////////////////////////////////////////////////////////////////////
  int32_t FromBigEndian(int32_t value)
  {
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = FromBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (uint64_t)
  /////////////////////////////////////////////////////////////////////////////
  uint64_t FromBigEndian(uint64_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint64_t>(bytes[0]) << 56) |
      (static_cast<uint64_t>(bytes[1]) << 48) |
      (static_cast<uint64_t>(bytes[2]) << 40) |
      (static_cast<uint64_t>(bytes[3]) << 32) |
      (static_cast<uint64_t>(bytes[4]) << 24) |
      (static_cast<uint64_t>(bytes[5]) << 16) |
      (static_cast<uint64_t>(bytes[6]) << 8) |
      (static_cast<uint64_t>(bytes[7]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (int64_t)
  /////////////////////////////////////////////////////////////////////////////
  int64_t FromBigEndian(int64_t value)
  {
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = FromBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (float)
  /////////////////////////////////////////////////////////////////////////////
  float FromBigEndian(float value)
  {
    static_assert(sizeof(float) == sizeof(uint32_t), "Size mismatch: float vs uint32_t");
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = FromBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromBigEndian (double)
  /////////////////////////////////////////////////////////////////////////////
  double FromBigEndian(double value)
  {
    static_assert(sizeof(double) == sizeof(uint64_t), "Size mismatch: double vs uint64_t");
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = FromBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (bool)
  /////////////////////////////////////////////////////////////////////////////
  bool FromLittleEndian(bool value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (uint8_t)
  /////////////////////////////////////////////////////////////////////////////
  uint8_t FromLittleEndian(uint8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (int8_t)
  /////////////////////////////////////////////////////////////////////////////
  int8_t FromLittleEndian(int8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (uint16_t)
  /////////////////////////////////////////////////////////////////////////////
  uint16_t FromLittleEndian(uint16_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint16_t>(bytes[1]) << 8) |
      (static_cast<uint16_t>(bytes[0]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (int16_t)
  /////////////////////////////////////////////////////////////////////////////
  int16_t FromLittleEndian(int16_t value)
  {
    uint16_t* uvalue = reinterpret_cast<uint16_t*>(&value);
    *uvalue = FromLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (uint32_t)
  /////////////////////////////////////////////////////////////////////////////
  uint32_t FromLittleEndian(uint32_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint32_t>(bytes[3]) << 24) |
      (static_cast<uint32_t>(bytes[2]) << 16) |
      (static_cast<uint32_t>(bytes[1]) << 8) |
      (static_cast<uint32_t>(bytes[0]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (int32_t)
  /////////////////////////////////////////////////////////////////////////////
  int32_t FromLittleEndian(int32_t value)
  {
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = FromLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (uint64_t)
  /////////////////////////////////////////////////////////////////////////////
  uint64_t FromLittleEndian(uint64_t value)
  {
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
    return(
      (static_cast<uint64_t>(bytes[7]) << 56) |
      (static_cast<uint64_t>(bytes[6]) << 48) |
      (static_cast<uint64_t>(bytes[5]) << 40) |
      (static_cast<uint64_t>(bytes[4]) << 32) |
      (static_cast<uint64_t>(bytes[3]) << 24) |
      (static_cast<uint64_t>(bytes[2]) << 16) |
      (static_cast<uint64_t>(bytes[1]) << 8) |
      (static_cast<uint64_t>(bytes[0]))
    );
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (int64_t)
  /////////////////////////////////////////////////////////////////////////////
  int64_t FromLittleEndian(int64_t value)
  {
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = FromLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (float)
  /////////////////////////////////////////////////////////////////////////////
  float FromLittleEndian(float value)
  {
    static_assert(sizeof(float) == sizeof(uint32_t), "Size mismatch: float vs uint32_t");
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = FromLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // FromLittleEndian (double)
  /////////////////////////////////////////////////////////////////////////////
  double FromLittleEndian(double value)
  {
    static_assert(sizeof(double) == sizeof(uint64_t), "Size mismatch: double vs uint64_t");
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = FromLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (bool)
  /////////////////////////////////////////////////////////////////////////////
  bool ToBigEndian(bool value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (uint8_t)
  /////////////////////////////////////////////////////////////////////////////
  uint8_t ToBigEndian(uint8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (int8_t)
  /////////////////////////////////////////////////////////////////////////////
  int8_t ToBigEndian(int8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (uint16_t)
  /////////////////////////////////////////////////////////////////////////////
  uint16_t ToBigEndian(uint16_t value)
  {
    uint16_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[0]       = static_cast<uint8_t>((value & 0xFF00) >> 8);
    bytes[1]       = static_cast<uint8_t>(value & 0x00FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (int16_t)
  /////////////////////////////////////////////////////////////////////////////
  int16_t ToBigEndian(int16_t value)
  {
    uint16_t* uvalue = reinterpret_cast<uint16_t*>(&value);
    *uvalue = ToBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (uint32_t)
  /////////////////////////////////////////////////////////////////////////////
  uint32_t ToBigEndian(uint32_t value)
  {
    uint32_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[0]       = static_cast<uint8_t>((value & 0xFF000000) >> 24);
    bytes[1]       = static_cast<uint8_t>((value & 0x00FF0000) >> 16);
    bytes[2]       = static_cast<uint8_t>((value & 0x0000FF00) >> 8);
    bytes[3]       = static_cast<uint8_t>(value & 0x000000FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (int32_t)
  /////////////////////////////////////////////////////////////////////////////
  int32_t ToBigEndian(int32_t value)
  {
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = ToBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (uint64_t)
  /////////////////////////////////////////////////////////////////////////////
  uint64_t ToBigEndian(uint64_t value)
  {
    uint64_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[0]       = static_cast<uint8_t>((value & 0xFF00000000000000) >> 56);
    bytes[1]       = static_cast<uint8_t>((value & 0x00FF000000000000) >> 48);
    bytes[2]       = static_cast<uint8_t>((value & 0x0000FF0000000000) >> 40);
    bytes[3]       = static_cast<uint8_t>((value & 0x000000FF00000000) >> 32);
    bytes[4]       = static_cast<uint8_t>((value & 0x00000000FF000000) >> 24);
    bytes[5]       = static_cast<uint8_t>((value & 0x0000000000FF0000) >> 16);
    bytes[6]       = static_cast<uint8_t>((value & 0x000000000000FF00) >> 8);
    bytes[7]       = static_cast<uint8_t>(value & 0x00000000000000FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (int64_t)
  /////////////////////////////////////////////////////////////////////////////
  int64_t ToBigEndian(int64_t value)
  {
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = ToBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (float)
  /////////////////////////////////////////////////////////////////////////////
  float ToBigEndian(float value)
  {
    static_assert(sizeof(float) == sizeof(uint32_t), "Size mismatch: float vs uint32_t");
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = ToBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToBigEndian (double)
  /////////////////////////////////////////////////////////////////////////////
  double ToBigEndian(double value)
  {
    static_assert(sizeof(double) == sizeof(uint64_t), "Size mismatch: double vs uint64_t");
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = ToBigEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (bool)
  /////////////////////////////////////////////////////////////////////////////
  bool ToLittleEndian(bool value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (uint8_t)
  /////////////////////////////////////////////////////////////////////////////
  uint8_t ToLittleEndian(uint8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (int8_t)
  /////////////////////////////////////////////////////////////////////////////
  int8_t ToLittleEndian(int8_t value)
  {
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (uint16_t)
  /////////////////////////////////////////////////////////////////////////////
  uint16_t ToLittleEndian(uint16_t value)
  {
    uint16_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[1]       = static_cast<uint8_t>((value & 0xFF00) >> 8);
    bytes[0]       = static_cast<uint8_t>(value & 0x00FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (int16_t)
  /////////////////////////////////////////////////////////////////////////////
  int16_t ToLittleEndian(int16_t value)
  {
    uint16_t* uvalue = reinterpret_cast<uint16_t*>(&value);
    *uvalue = ToLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (uint32_t)
  /////////////////////////////////////////////////////////////////////////////
  uint32_t ToLittleEndian(uint32_t value)
  {
    uint32_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[3]       = static_cast<uint8_t>((value & 0xFF000000) >> 24);
    bytes[2]       = static_cast<uint8_t>((value & 0x00FF0000) >> 16);
    bytes[1]       = static_cast<uint8_t>((value & 0x0000FF00) >> 8);
    bytes[0]       = static_cast<uint8_t>(value & 0x000000FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (int32_t)
  /////////////////////////////////////////////////////////////////////////////
  int32_t ToLittleEndian(int32_t value)
  {
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = ToLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (uint64_t)
  /////////////////////////////////////////////////////////////////////////////
  uint64_t ToLittleEndian(uint64_t value)
  {
    uint64_t result;
    uint8_t* bytes = reinterpret_cast<uint8_t*>(&result);
    bytes[7]       = static_cast<uint8_t>((value & 0xFF00000000000000) >> 56);
    bytes[6]       = static_cast<uint8_t>((value & 0x00FF000000000000) >> 48);
    bytes[5]       = static_cast<uint8_t>((value & 0x0000FF0000000000) >> 40);
    bytes[4]       = static_cast<uint8_t>((value & 0x000000FF00000000) >> 32);
    bytes[3]       = static_cast<uint8_t>((value & 0x00000000FF000000) >> 24);
    bytes[2]       = static_cast<uint8_t>((value & 0x0000000000FF0000) >> 16);
    bytes[1]       = static_cast<uint8_t>((value & 0x000000000000FF00) >> 8);
    bytes[0]       = static_cast<uint8_t>(value & 0x00000000000000FF);
    return result;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (int64_t)
  /////////////////////////////////////////////////////////////////////////////
  int64_t ToLittleEndian(int64_t value)
  {
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = ToLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (float)
  /////////////////////////////////////////////////////////////////////////////
  float ToLittleEndian(float value)
  {
    static_assert(sizeof(float) == sizeof(uint32_t), "Size mismatch: float vs uint32_t");
    uint32_t* uvalue = reinterpret_cast<uint32_t*>(&value);
    *uvalue = ToLittleEndian(*uvalue);
    return value;
  }

  /////////////////////////////////////////////////////////////////////////////
  // ToLittleEndian (double)
  /////////////////////////////////////////////////////////////////////////////
  double ToLittleEndian(double value)
  {
    static_assert(sizeof(double) == sizeof(uint64_t), "Size mismatch: double vs uint64_t");
    uint64_t* uvalue = reinterpret_cast<uint64_t*>(&value);
    *uvalue = ToLittleEndian(*uvalue);
    return value;
  }

} // namespace ByteOrdering
