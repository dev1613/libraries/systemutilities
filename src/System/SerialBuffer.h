/**
 * @file SerialBuffer.h
 *
 * @brief Classes to represent serial buffer
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <System/ByteOrdering.h>

namespace System
{
  // Forward declarations
  class Serializable;

  //-------------------------------------------------------------------------//
  // Class:       MsgHeader
  // Description: Header for all messages sent with SerialBuffer
  //-------------------------------------------------------------------------//
  struct MsgHeader
  {
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    MsgHeader() : length(0), id(0) {}

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~MsgHeader() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Converts the host-based header to network format
    /// @return - header in network format
    ///////////////////////////////////////////////////////////////////////////
    MsgHeader hostToNetwork() const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Converts the network-based header to host format
    /// @return - header in host format
    ///////////////////////////////////////////////////////////////////////////
    MsgHeader networkToHost() const;

    // Struct variables
    uint32_t length; ///< message length
    uint32_t id;     ///< message ID
  };

  //-------------------------------------------------------------------------//
  // Class:       SerialBuffer
  // Description: Buffer for sending information between applications
  //-------------------------------------------------------------------------//
  class SerialBuffer
  {
  public:
    // 4 MB
    static constexpr size_t DEFAULT_SIZE = 4 * 1024 * 1024;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param size - initial buffer size to allocate
    ///////////////////////////////////////////////////////////////////////////
    SerialBuffer(size_t size = DEFAULT_SIZE);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~SerialBuffer() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Appends the data from the buffer into this buffer
    /// @param data - pointer to start of data
    /// @param count - number of bytes
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void append(const uint8_t* data, size_t count);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Appends the string to the buffer
    /// @param str - string
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void append(const std::string& str);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Appends the primitive value to the buffer
    /// @param value - value to append
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    void append(const T& value)
    {
      static_assert(std::is_arithmetic<T>::value, "Must be arithmetic type");
      auto swapValue = ByteOrdering::ToBigEndian(value);
      append(reinterpret_cast<const uint8_t*>(&swapValue), sizeof(T));
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the message buffer
    /// @return - message buffer
    ///////////////////////////////////////////////////////////////////////////
    const std::vector<uint8_t>& buffer() const { return mBuffer; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Clears all data from the buffer except the header
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void clear();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the message ID
    /// @return - message ID
    ///////////////////////////////////////////////////////////////////////////
    uint32_t msgID();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Reads data from the buffer
    /// @param data - pointer to start of data
    /// @param count - number of bytes
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool read(uint8_t* data, size_t count);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Reads a string from the buffer
    /// @param str - string
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool read(std::string& str);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Reads the primitive value from the buffer
    /// @param value - value to read
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    template <typename T>
    bool read(T& value)
    {
      static_assert(std::is_arithmetic<T>::value, "Must be arithmetic type");
      bool valid = false;
      if(read(reinterpret_cast<uint8_t*>(&value), sizeof(T)))
      {
        value = ByteOrdering::FromBigEndian(value);
        valid = true;
      }

      return valid;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the message ID
    /// @param id - ID to set
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setMsgID(uint32_t id);

  private:
    // Private class variables
    std::vector<uint8_t> mBuffer;    ///< data buffer
    size_t               mReadIndex; ///< index in buffer to read

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the number of bytes remaining to be read
    /// @return - number of bytes remaining
    ///////////////////////////////////////////////////////////////////////////
    size_t numReadBytesRemaining() const;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the message header
    /// @return - pointer to message header
    ///////////////////////////////////////////////////////////////////////////
    MsgHeader* getHeader();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Updates the message length
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void updateMsgLength();
  };

  // Types
  using sSerialBuffer = std::shared_ptr<SerialBuffer>;

} // namespace System
