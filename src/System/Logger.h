/**
 * @file Logger.h
 *
 * @brief Classes to represent debug logger
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <iostream>
#include <memory>

namespace System
{
  //-------------------------------------------------------------------------//
  // Class:       Logger
  // Description: Debug logger
  //-------------------------------------------------------------------------//
  enum LogLevel
  {
    LOG_DEBUG,
    LOG_INFO,
    LOG_WARN,
    LOG_ERROR,
    LOG_FATAL
  };

  /////////////////////////////////////////////////////////////////////////////
  /// @brief Translates string to log level
  /// @param level[out] - log level
  /// @param levelStr - log level string
  /// @return - true if valid, else false
  /// @param out - output stream
  /// @param level - minimum logging level
  /////////////////////////////////////////////////////////////////////////////
  bool GetLogLevel(LogLevel& level, const std::string& levelStr);

  //-------------------------------------------------------------------------//
  // Class:       Logger
  // Description: Debug logger
  //-------------------------------------------------------------------------//
  class Logger
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param out - output stream
    /// @param level - minimum logging level
    ///////////////////////////////////////////////////////////////////////////
    Logger(std::ostream& out = std::cout, LogLevel level = LOG_INFO);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Logger() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the DEBUG level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void debug(const char* format, Args&&... args)
    {
      log(LOG_DEBUG, format, std::forward<Args>(args)...);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the ERROR level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void error(const char* format, Args&&... args)
    {
      log(LOG_ERROR, format, std::forward<Args>(args)...);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the FATAL level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void fatal(const char* format, Args&&... args)
    {
      log(LOG_FATAL, format, std::forward<Args>(args)...);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the INFO level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void info(const char* format, Args&&... args)
    {
      log(LOG_INFO, format, std::forward<Args>(args)...);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the specified level
    /// @param level - log level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void log(LogLevel level, const char* format, ...);

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the logging level
    /// @param level - logging level
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void setLevel(LogLevel level) { mLevel = level; }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Logs a message at the WARN level
    /// @param format - string format
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void warn(const char* format, Args&&... args)
    {
      log(LOG_WARN, format, std::forward<Args>(args)...);
    }

  private:
    // Private class variables
    std::ostream& mOut;   ///< output stream
    LogLevel      mLevel; ///< minimum logging level
  };

  // Types
  using sLogger = std::shared_ptr<Logger>;

} // namespace System
