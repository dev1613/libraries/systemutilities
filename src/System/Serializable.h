/**
 * @file Serializable.h
 *
 * @brief Classes to represent serializable object
 *
 * @ingroup System Utilities
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <cstdint>
#include <string>
#include <vector>

namespace System
{
  // Forward declarations
  class SerialBuffer;

  //-------------------------------------------------------------------------//
  // Class:       Serializable
  // Description: Base class for serializable objects
  //-------------------------------------------------------------------------//
  class Serializable
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    Serializable() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Serializable() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Decodes the member data from the buffer
    /// @param buffer - buffer to append to
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    virtual bool decode(SerialBuffer& buffer) = 0;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Encodes the member data into the buffer
    /// @param buffer - buffer to append to
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    virtual void encode(SerialBuffer& buffer) const = 0;
  };

  // Typedefs
  using sSerializable = std::shared_ptr<Serializable>;

} // namespace System
