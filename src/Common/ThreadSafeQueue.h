/**
 * @file ThreadSafeQueue.h
 *
 * @brief Wrapper class for mutex-protected data for a given type
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>

namespace Common
{
  //-------------------------------------------------------------------------//
  // Class:       ThreadSafeQueue
  // Description: Wrapper class for mutex-protected queue
  // Templates:   T - queue data type
  //-------------------------------------------------------------------------//
  template <typename T>
  class ThreadSafeQueue
  {
  public:
    // Public types
    using ExitFunction = std::function<bool()>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    ThreadSafeQueue() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ThreadSafeQueue()
    {
      notify();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Copy/move constructors and operators (deleted)
    ///////////////////////////////////////////////////////////////////////////
    ThreadSafeQueue(const ThreadSafeQueue&) = delete;
    ThreadSafeQueue(ThreadSafeQueue&&) = delete;
    ThreadSafeQueue& operator=(const ThreadSafeQueue&) = delete;
    ThreadSafeQueue& operator=(ThreadSafeQueue&&) = delete;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds an item to the queue
    /// @param item - item to add
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void add(const T& item)
    {
      {
        std::lock_guard<std::mutex> theLock(mMutex);
        mData.push(item);
      }

      mCondition.notify_one();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds an item to the queue
    /// @param item - item to add
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void add(T&& item)
    {
      {
        std::lock_guard<std::mutex> theLock(mMutex);
        mData.push(std::move(item));
      }

      mCondition.notify_one();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Notifies the waiting getter to wake up
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void notify()
    {
      mCondition.notify_all();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Removes an item from the queue
    /// @param item[out] - removed item
    /// @param waitOnEmpty - wait for item to be added
    /// @param exitCriteria - criteria to exit wait
    /// @return - true if item was removed, else false
    ///////////////////////////////////////////////////////////////////////////
    bool remove(T& item, bool waitOnEmpty = true, const ExitFunction& exitCriteria = []() { return false; })
    {
      bool success = false;

      {
        std::unique_lock<std::mutex> theLock(mMutex);

        // Wait for items to be added
        if(waitOnEmpty && mData.empty() && !exitCriteria())
        {
          mCondition.wait(theLock, [this, exitCriteria]()
          {
            return(!mData.empty() || exitCriteria());
          });
        }

        // Remove item from queue
        success = !mData.empty();
        if(success)
        {
          item = mData.front();
          mData.pop();
        }
      }

      return success;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Removes an item from the queue
    /// @param item[out] - removed item
    /// @param timeout - timeout to wait if empty
    /// @param exitCriteria - criteria to exit wait
    /// @return - true if item was removed, else false
    ///////////////////////////////////////////////////////////////////////////
    template <class _Rep, class _Period>
    bool remove(T& item, std::chrono::duration<_Rep, _Period> timeout, const ExitFunction& exitCriteria = []() { return false; })
    {
      bool success = false;

      {
        std::unique_lock<std::mutex> theLock(mMutex);

        // Wait for items to be added
        if(mData.empty() && !exitCriteria())
        {
          mCondition.wait_for(theLock, timeout, [this, exitCriteria]()
          {
            return(!mData.empty() || exitCriteria);
          });
        }

        // Remove item from queue
        success = !mData.empty();
        if(success)
        {
          item = mData.front();
          mData.pop();
        }
      }

      // Remove item from queue
      return success;
    }

  private:
    // Private types
    using QueueType    = std::queue<T>;
    using DataFunction = std::function<void(QueueType&)>;

    // Private class variables
    QueueType               mData;      ///< internal data
    std::mutex              mMutex;     ///< data mutex
    std::condition_variable mCondition; ///< condition variable to notify update
  };

} // namespace Common
