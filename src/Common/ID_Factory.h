/**
 * @file ID_Factory.h
 *
 * @brief Represents a factory class for ID numbers
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <algorithm>
#include <cstdint>
#include <limits>
#include <list>

#include <Common/ThreadSafeData.h>

namespace Common
{
  namespace details
  {
    //-----------------------------------------------------------------------//
    // Class:       FreeChunk
    // Description: Internal type for chunk of free IDs
    // Templates:   T - chunk type (must be integral type)
    //-----------------------------------------------------------------------//
    template <typename T = uint32_t>
    class FreeChunk
    {
    public:
      // Constants
      static_assert(std::is_integral<T>::value, "FreeChunk must use integer types");

      static constexpr T ONE = static_cast<T>(1);

      /////////////////////////////////////////////////////////////////////////
      /// @brief Constructor
      /// @param start - chunk start
      /// @param end - chunk end
      /////////////////////////////////////////////////////////////////////////
      FreeChunk(T start = ONE, T end = ONE) :
        mStart(std::min(start, end)),
        mEnd(std::max(start, end))
      {
      }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Destructor
      /////////////////////////////////////////////////////////////////////////
      virtual ~FreeChunk() = default;

      /////////////////////////////////////////////////////////////////////////
      /// @brief Checks if the chunk contains the requested ID
      /// @param id - ID to check
      /// @return - true if this chunk contains the ID, else false
      /////////////////////////////////////////////////////////////////////////
      bool contains(T id) const { return(id >= mStart && id < mEnd); }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Checks if the chunk is empty
      /// @return - true if empty, else false
      /////////////////////////////////////////////////////////////////////////
      bool empty() const { return(mStart == mEnd); }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Gets the end of this chunk
      /// @return - chunk end
      /////////////////////////////////////////////////////////////////////////
      T end() const { return mEnd; }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Joins this chunk with another
      /// @param other - other chunk to join
      /// @return - true if joined, else false
      /////////////////////////////////////////////////////////////////////////
      bool join(FreeChunk& other)
      {
        bool success = true;
        if(mEnd == other.mStart)
        {
          mEnd = other.mEnd;
          other.mStart = other.mEnd;
        }
        else if(mStart == other.mEnd)
        {
          mStart = other.mStart;
          other.mEnd = other.mStart;
        }
        else
        {
          success = false;
        }

        return success;
      }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Gets the chunk size
      /// @return - chunk size
      /////////////////////////////////////////////////////////////////////////
      T size() const { return(mEnd - mStart); }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Gets the start of this chunk
      /// @return - chunk start
      /////////////////////////////////////////////////////////////////////////
      T start() const { return mStart; }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Splits the chunk at the requested ID
      /// @param id - requested ID
      /// @param nextChunk[out] - second half of chunk once split
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      bool split(T id, FreeChunk& nextChunk)
      {
        bool success = contains(id);
        if(success)
        {
          nextChunk.mStart = id + ONE;
          nextChunk.mEnd   = mEnd;
          mEnd             = id;
        }

        return success;
      }

      /////////////////////////////////////////////////////////////////////////
      /// @brief Takes the next ID from the chunk
      /// @param id[out] - next ID
      /// @return - true if successful, else false
      /////////////////////////////////////////////////////////////////////////
      bool takeNext(T& id)
      {
        bool success = !empty();
        if(success)
        {
          id = mStart++;
        }

        return success;
      }

    private:
      // Private class variables
      T mStart; ///< start of free chunk (inclusive)
      T mEnd;   ///< end of free chunk (exclusive)
    };

  } // namespace details

  //-------------------------------------------------------------------------//
  // Class:       ID_Factory
  // Description: Factory of unique IDs
  // Templates:   T - factory type (must be integral type)
  //-------------------------------------------------------------------------//
  template <typename T = uint32_t>
  class ID_Factory
  {
  public:
    // Constants
    static_assert(std::is_integral<T>::value, "ID_Factory must use integer types");

    static constexpr T MIN_ID = std::numeric_limits<T>::min();
    static constexpr T MAX_ID = std::numeric_limits<T>::max();

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    ///////////////////////////////////////////////////////////////////////////
    ID_Factory()
    {
      mFreeChunks.set([](Chunks& freeChunks)
      {
        freeChunks.emplace_back(MIN_ID, MAX_ID);
      });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ID_Factory() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Releases the requested ID
    /// @param id - requested ID
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool release(T id)
    {
      bool success = false;
      mFreeChunks.set([&success, id](Chunks& freeChunks)
      {
        // Find next chunk
        auto prevChunk = freeChunks.end();
        auto nextChunk = freeChunks.end();
        for(auto it = freeChunks.begin(); it != freeChunks.end(); ++it)
        {
          if(it->contains(id))
          {
            // Error case
            return;
          }
          else if(id < it->start())
          {
            prevChunk = nextChunk;
            nextChunk = it;
            break;
          }
        }

        // Add to free chunks
        ChunkType newChunk(id, id + ChunkType::ONE);
        if((prevChunk != freeChunks.end() && prevChunk->join(newChunk)) ||
           (nextChunk != freeChunks.end() && nextChunk->join(newChunk)))
        {
          // New chunk was appended to existing. Try joining the 2 together
          if(prevChunk != freeChunks.end() && nextChunk != freeChunks.end() &&
             prevChunk->join(*nextChunk))
          {
            // If previous was joined with next, remove next
            freeChunks.erase(nextChunk);
          }
        }
        else
        {
          // Couldn't join with existing chunk. Add new
          freeChunks.insert(nextChunk, newChunk);
        }

        success = true;
      });

      return success;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Takes the requested ID
    /// @param id - requested ID
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool take(T id)
    {
      bool success = false;
      mFreeChunks.set([&success, id](Chunks& freeChunks)
      {
        for(auto it = freeChunks.begin(); !success && it != freeChunks.end();)
        {
          ChunkType next;
          success = it->split(id, next);
          if(success)
          {
            if(it->empty() && next.empty())
            {
              // Case 1: Took last ID from empty chunk
              freeChunks.erase(it);
            }
            else if(it->empty() && !next.empty())
            {
              // Case 2: Removed first ID from chunk
              *it = next;
            }
            else if(!it->empty() && !next.empty())
            {
              // Case 3: Split into 2 non-empty chunks
              freeChunks.insert(std::next(it), next);
            }

            // Case 4: Took last ID from chunk (second half is empty)
          }
          else
          {
            ++it;
          }
        }
      });

      return success;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Takes the next available ID
    /// @param id[out] - next ID
    /// @return - true if successful, else false
    ///////////////////////////////////////////////////////////////////////////
    bool takeNext(T& id)
    {
      bool success = false;
      mFreeChunks.set([&success, &id](Chunks& freeChunks)
      {
        auto it = freeChunks.begin();
        if(it != freeChunks.end())
        {
          success = it->takeNext(id);
          if(success && it->empty())
          {
            freeChunks.erase(it);
          }
        }
      });

      return success;
    }

  private:
    // Private types
    using ChunkType = details::FreeChunk<T>;
    using Chunks    = std::list<ChunkType>;

    // Private class variables
    ThreadSafeData<Chunks> mFreeChunks; ///< linked list of free chunks

  };

} // namespace Common
