/**
 * @file ThreadSafeData.h
 *
 * @brief Wrapper class for mutex-protected data for a given type
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>

namespace Common
{
  //-------------------------------------------------------------------------//
  // Class:       ThreadSafeData
  // Description: Wrapper class for mutex-protected data
  // Templates:   T - internal data type
  //-------------------------------------------------------------------------//
  template <typename T>
  class ThreadSafeData
  {
  public:
    // Public types
    using DataFunction = std::function<void(T&)>;
    using ExitFunction = std::function<bool()>;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param args - args to construct initial data
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    ThreadSafeData(Args&&... args) : mData(std::forward(args)...), mUpdate(false)
    {
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~ThreadSafeData()
    {
      notify();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Copy/move constructors and operators (deleted)
    ///////////////////////////////////////////////////////////////////////////
    ThreadSafeData(const ThreadSafeData&) = delete;
    ThreadSafeData(ThreadSafeData&&) = delete;
    ThreadSafeData& operator=(const ThreadSafeData&) = delete;
    ThreadSafeData& operator=(ThreadSafeData&&) = delete;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the data
    /// @param data - new data
    /// @param timeout - time to stop waiting
    /// @param exitCriteria - criteria to exit wait
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <class _Rep, class _Period>
    void get(T& data, std::chrono::duration<_Rep, _Period> timeout, const ExitFunction& exitCriteria = []() { return false; })
    {
      auto func = [&data](T& theData)
      {
        data = theData;
      };

      get(func, timeout, exitCriteria);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the data
    /// @param data - new data
    /// @param waitForUpdate - if true, wait for update to be notified
    /// @param exitCriteria - criteria to exit wait
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void get(T& data, bool waitForUpdate = true, const ExitFunction& exitCriteria = []() { return false; })
    {
      auto func = [&data](T& theData)
      {
        data = theData;
      };

      get(func, waitForUpdate, exitCriteria);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the data
    /// @param data - new data
    /// @param timeout - time to stop waiting
    /// @param exitCriteria - criteria to exit wait
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <class _Rep, class _Period>
    void get(const DataFunction& func, std::chrono::duration<_Rep, _Period> timeout, const ExitFunction& exitCriteria = []() { return false; })
    {
      std::unique_lock<std::mutex> theLock(mMutex);

      if(!mUpdate && !exitCriteria())
      {
        mCondition.wait_for(theLock, timeout, [this, exitCriteria]()
        {
          return(mUpdate || exitCriteria());
        });
      }

      mUpdate = false;
      func(mData);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets the data
    /// @param func - function to call to retrieve data
    /// @param waitForUpdate - if true, wait for update to be notified
    /// @param exitCriteria - criteria to exit wait
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void get(const DataFunction& func, bool waitForUpdate = true, const ExitFunction& exitCriteria = []() { return false; })
    {
      std::unique_lock<std::mutex> theLock(mMutex);

      if(!mUpdate && waitForUpdate && !exitCriteria())
      {
        mCondition.wait(theLock, [this, exitCriteria]()
        {
          return(mUpdate || exitCriteria());
        });
      }

      mUpdate = false;
      func(mData);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Notifies the waiting getter to wake up
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void notify()
    {
      mCondition.notify_all();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the data
    /// @param data - new data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void set(T&& data)
    {
      set([data = std::move(data)](T& theData)
      {
        theData = std::move(data);
      });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the data
    /// @param data - new data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void set(const T& data)
    {
      set([&data](T& theData)
      {
        theData = data;
      });
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Sets the data
    /// @param func - function to update data
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    void set(const DataFunction& func)
    {
      {
        std::lock_guard<std::mutex> theLock(mMutex);
        func(mData);
        mUpdate = true;
      }

      mCondition.notify_one();
    }

  private:
    T                       mData;      ///< internal data
    bool                    mUpdate;    ///< data updated?
    std::mutex              mMutex;     ///< data mutex
    std::condition_variable mCondition; ///< condition variable to notify update
  };

} // namespace Common
