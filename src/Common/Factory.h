/**
 * @file Factory.h
 *
 * @brief Represents a factory class for a given type
 *
 * @ingroup CommonTypes
 *
 * @author Devon Johnson
 *
 */

#pragma once

// Includes
#include <list>
#include <memory>

#include <Common/ThreadSafeData.h>

namespace Common
{
  //-------------------------------------------------------------------------//
  // Class:       Factory
  // Description: Factory type for unique instances of a given type
  // Templates:   T - factory type
  //-------------------------------------------------------------------------//
  template <typename T>
  class Factory
  {
  public:
    ///////////////////////////////////////////////////////////////////////////
    /// @brief Constructor
    /// @param maxInstances - maximum number of instances
    ///////////////////////////////////////////////////////////////////////////
    Factory(size_t maxInstances = std::numeric_limits<size_t>::max()) :
      mMaxInstances(maxInstances)
    {
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Destructor
    ///////////////////////////////////////////////////////////////////////////
    virtual ~Factory() = default;

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Gets all instances
    /// @return - all instances currently in factory
    ///////////////////////////////////////////////////////////////////////////
    std::list<std::shared_ptr<const T>> allInstances()
    {
      std::list<std::shared_ptr<const T>> instances;
      auto func = [&instances](Instances& factory)
      {
        instances.assign(factory.begin(), factory.end());
      };

      mFactory.get(func, false);

      return std::move(instances);
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Find an instance
    /// @param pred - predicate to match
    /// @return - instance, or nullptr if not found
    ///////////////////////////////////////////////////////////////////////////
    std::shared_ptr<T> find(const std::function<bool(const T&)>& pred)
    {
      // Wrapper predicate
      auto localPred = [&pred](const std::shared_ptr<T>& ptr)
      {
        return pred(*ptr);
      };

      // Function to get instance from factory
      std::shared_ptr<T> pInstance;
      auto func = [&localPred, &pInstance](Instances& factory)
      {
        auto it = std::find_if(factory.begin(), factory.end(), localPred);
        if(it != factory.end())
        {
          pInstance = *it;
        }
      };

      // Get the instance
      mFactory.get(func, false);
      return pInstance;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Creates a new instance
    /// @param args - arguments to create type
    /// @return - instance created, or nullptr if failed
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    std::shared_ptr<T> create(Args&&... args)
    {
      using namespace std::placeholders;
      std::shared_ptr<T> pInstance;
      mFactory.set(std::bind(&Factory<T>::addInstance<Args...>, this, _1,
                   std::ref(pInstance), std::forward<Args>(args)...));

      return pInstance;
    }

  private:
    // Private types
    using Instances = std::list<std::shared_ptr<T>>;

    // Private class variables
    ThreadSafeData<Instances> mFactory; ///< factory instances

    const size_t mMaxInstances; ///< maximum number of instances

    ///////////////////////////////////////////////////////////////////////////
    /// @brief Adds an instance
    /// @param factory - current factory instances
    /// @param pInstance[out] - new created instance
    /// @param args - additional arguments to create type
    /// @return - None
    ///////////////////////////////////////////////////////////////////////////
    template <typename... Args>
    void addInstance(Instances& factory, std::shared_ptr<T>& pInstance, Args&&... args)
    {
      if(factory.size() < mMaxInstances)
      {
        auto* ptr = new T(std::forward<Args>(args)...);
        pInstance.reset(ptr);

        if(pInstance)
        {
          factory.push_back(pInstance);
        }
      }
      else
      {
        pInstance = nullptr;
      }
    }
  };
  
} // namespace Phase10
